<?php

// Set display errors
error_reporting(0);

/**
 * LICENSE: By using the software, you agree to be bound by the terms of this license.
 * You must have purchase it from roboscripts.net to use the software.
 * The copyright holder retains all intellectual property rights to the software.
 * You are granted the right to install the software on one website.
 * You may not re-distribute (either freely, or for any kind of compensation) the software, 
 * in whole or in part, to any 3rd party. You expressly acknowledge and agree that use of
 * the software is at your sole risk. No warrantly is provided, either express or implied.
 *
 * @copyright 2016 roboscripts.net
 * @license See license agreement in license.txt
 * @version 2.00
 * @link http://www.roboscripts.net/
 * @file available since Release 1.00
 */

////////////////////////////////////////////////////////////////////////////////////
//                                                                                //
//                   (1) Function to check if curl is installed                   //
//                                                                                //
//                            - @return true or false;                            //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

function _iscurlinstalled() 
{
	if (in_array('curl', get_loaded_extensions())) {
		return true;
	} 

	return false;
}

////////////////////////////////////////////////////////////////////////////////////
//                                                                                //
//                (2) Function to check if mod_rewrite is available               //
//                                                                                //
//                           - @return true or false;                             //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

function is_mod_rewrite() 
{
	if(!function_exists('apache_get_modules') ){ 
		return false;
	}
	if(in_array('mod_rewrite',apache_get_modules())){ 
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////////
//                                                                                //
//                    (3) Function to validate the PHP version                    //
//                                                                                //
//                           - @return true or false;                             //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

function validate_php_version($data) 
{
	if(strpos($data, '5.2')!==false || strpos($data, '5.3')!==false || strpos($data, '5.4')!==false 
	|| strpos($data, '5.5')!==false || strpos($data, '5.6')!==false || strpos($data, '7')!==false){
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////////
//                                                                                //
//                   (4) Function to validate IconCube version                    //
//                                                                                //
//                           - @return true or false;                             //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

function validate_ion_cube_version($data) 
{
	if($data[0] >= 4){
		return true;
	}

	return false;
}

?>
<!doctype html public "-//W3C//DTD HTML 4.0 //EN"> 
<html>
<head>
<title>Robo Scripts &raquo; Minimum Server Requirements Tester</title>
<meta name="robots" content="noindex, nofollow">
<style type="text/css">
body {
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 13px; 
  color: #545353;
  margin: 0;
  padding-bottom: 50px;
}
table {
  border: none;
  border-collapse: collapse;
}
.header {
  background: #428BCA;
  background: -webkit-gradient(linear,left top,left bottom,from(#428BCA),to(#326898));
  background: -webkit-linear-gradient(top,#428BCA,#326898);
  background: -moz-linear-gradient(top,#428BCA,#326898);
  background: -ms-linear-gradient(top,#428BCA,#326898);
  background: -o-linear-gradient(top,#428BCA,#326898);
  background: linear-gradient(top,#428BCA,#326898);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorStr='#428BCA',EndColorStr='#326898');
  padding-top: 8px; 
  padding-bottom: 13px;
  margin-top: 0px;
  margin-bottom: 40px;
}
.header .header-text-big {
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 16px;
  color: #eee;
  font-weight: bold;
}
.header .header-text-small {
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 14px;
  color: #ccc; 
  font-weight: normal; 
}
#server-infos tr:hover {
  background-color: #ccc;
}
#server-infos tr:hover td {
  color: #000;
}
#server-infos {
  border-bottom: 1px solid #B3B3B3; 
  border-left: 1px solid #B3B3B3; 
  border-right: 1px solid #B3B3B3;
}
#server-infos td {
  border-bottom: 1px solid #eee; 
}
#server-infos .last-tr .text-bottom {
  border-bottom: 1px solid #B3B3B3; 
}
#server-infos tr {
  border-left: 1px solid #B3B3B3; 
  border-right: 1px solid #B3B3B3;
}
td {
  font-size: 11px;
  padding-top: 5px;
  padding-bottom: 5px;
}
td.cron-command {
  font-size: 12px;
  padding-top: 8px;
  padding-bottom: 8px;
}
th {
  background-color: #eee;
  font-size: 11px; 
  text-align: left; 
  padding-right: 30px; 
  border-bottom: 1px solid #2B2B2B; 
  border-top: 1px solid #2B2B2B; 
  cursor: pointer;
  background: #404040;
  background:-moz-linear-gradient(top, #808080 0%, #555555 2%, #404040 100%);
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0%,#808080), color-stop(2%,#555555), color-stop(100%,#404040));
  background:-webkit-linear-gradient(top, #808080 0%, #555555 2%,#404040 100%);
  background:-o-linear-gradient(top, #808080 0%, #555555 2%,#404040 100%);
  background:-ms-linear-gradient(top, #808080 0%, #555555 2%,#404040 100%);
  background:linear-gradient(top, #808080 0%, #555555 2%,#404040 100%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#555555', endColorstr='#404040');
  -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr='#555555', endColorstr='#404040')";
  -moz-box-shadow: inset 0 1px 0 rgba(255,255,255,0.3);
  -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.3);
  box-shadow: inset 0 1px 0 rgba(255,255,255,0.3);
  color: #fff !important; 
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.33); 
  -webkit-font-smoothing: antialiased;
}
th:first-child {
  border-left: 1px solid #2B2B2B; 
}
th:last-child {
  border-right: 1px solid #2B2B2B; 
}
.darker {
  background-color: #f1f1f1;
}
#space-left {
  padding-left: 10px;
} 
.positive {
  color: green;  
  font-weight: bold;
}
.negative {
  color: red; 
  font-weight: bold;
}
.success {
  color: green; 
  font-weight: bold; 
  padding-top: 10px; 
  padding-bottom: 10px;
}
.warning {
  color: red; 
  font-weight: bold; 
  padding-top: 10px; 
  padding-bottom: 10px;
}
.alert {
  margin-top: 20px;
  border: 1px solid transparent;
  border-radius: 4px;
  padding-left: 0px;
  padding-right: 30px;
}
.alert-success {
  background-color: #dff0d8;
  border-color: #d1e1c1;
  color: #3c763d;
}
.alert-warning {
  background-color: #fcf8e3;
  border-color: #faebcc;
  color: #8a6d3b;
}
.alert-danger {
  background-color: #f2dede;
  border-color: #ebccd1;
  color: #a94442;
}
.alert li {
  font-size: 13px; 
  font-weight: normal; 
  margin-bottom: 10px;
}
.cron-table {
  margin-top: 22px;
}
</style>
</head>
<body>
  <div align="center">
    <div class="header" align="center">
      <span class="header-text-big">Robo Scripts <span class="header-text-small">Minimum Server Requirements Tester</span></span>
    </div>		
    <table id="server-infos" cellpadding="3">
      <tr>
        <th id="space-left" width="200">Componants</th>
 	 <th width="200">Message</th>
	 <th width="200">Version</th>
	 <th width="30">Status</th>
      </tr>
      <tr>
        <td align="left" id="space-left" width="200">PHP</td>
        <td align="left" width="200">PHP is installed</td>
        <td align="left" id="space-left" width="200"><?php echo phpversion(); ?></td>
        <td align="left" id="space-left" width="30"><?php if(validate_php_version(phpversion())){echo '<div class="positive">&#8730;</div>';} else {echo '<div class="negative">&#935;</div>';} ?></td>
      </tr>
      <tr>
        <td align="left" class="darker" id="space-left" width="200">Ion Cube Loader</td>
	 <td align="left" class="darker" width="200"><?php if(!function_exists('ioncube_loader_version')){$ion_status = false; echo 'Ion Cube Loader is NOT installed';} else {$ion_status = true; echo 'Ion Cube is installed';} ?></td>
	 <td align="left" class="darker" id="space-left" width="200"><?php if($ion_status){echo ioncube_loader_version();} ?></td>
	 <td align="left" class="darker" id="space-left" width="30"><?php if($ion_status){echo '<div class="positive">&#8730;</div>';} else {echo '<div class="negative">&#935;</div>';} ?></td>
      </tr>
      <tr>
        <td align="left" id="space-left" width="200">MySQL</td>
	 <td align="left" width="200"><?php if(!function_exists('mysql_connect') && !function_exists('mysqli_connect')){$mysql_status = false; echo 'MySQL is not installed';} else {$mysql_status = true; echo 'MySQL is installed';} ?></td>
	 <td align="left" id="space-left" width="200">N/A</td>
	 <td align="left" id="space-left" width="30"><?php if($mysql_status){echo '<div class="positive">&#8730;</div>';} else {echo '<div class="negative">&#935;</div>';} ?></td>
      </tr>
      <tr>
        <td align="left" class="darker" id="space-left" width="200">PHP cURL extension</td>
	 <td align="left" class="darker" width="200"><?php if (_iscurlinstalled()){$curl_status = true; echo 'PHP cURL extension is installed';} else {$curl_status = false; echo 'PHP cURL extension is NOT installed';} ?></td>
  	 <td align="left" class="darker" id="space-left" width="200">N/A</td>
	 <td align="left" class="darker" id="space-left" width="30"><?php if($curl_status){echo '<div class="positive">&#8730;</div>';} else {echo '<div class="negative">&#935;</div>';} ?></td>
      </tr>
      <tr>
        <td align="left" id="space-left" width="200">PHP fopen()</td>
	 <td align="left" width="200"><?php if(!function_exists('fopen')){$fopen_status = false; echo 'PHP fopen() is disabled';} else {$fopen_status = true; echo 'PHP fopen() is enabled';} ?></td>
	 <td align="left" id="space-left" width="200">N/A</td>
	 <td align="left" id="space-left" width="30"><?php if($fopen_status){echo '<div class="positive">&#8730;</div>';} else {echo '<div class="negative">&#935;</div>';} ?></td>
      </tr>
      <tr class="last-tr">
        <td align="left" class="darker text-bottom" id="space-left" width="200">PHP file_get_contents()</td>
	 <td align="left" class="darker text-bottom" width="200"><?php if(!function_exists('file_get_contents')){$file_gc_status = false; echo 'PHP file_get_contents() is disabled';} else {$file_gc_status = true; echo 'PHP file_get_contents() is enabled';} ?></td>
	 <td align="left" class="darker text-bottom" id="space-left" width="200">N/A</td>
	 <td align="left" class="darker text-bottom" id="space-left" width="30"><?php if($file_gc_status){echo '<div class="positive">&#8730;</div>';} else {echo '<div class="negative">&#935;</div>';} ?></td>
      </tr>
    </table>

    <?php

      if(!$mysql_status) {

		echo '<div align="center">
               	<table width="780">
                 	<tr>
                   	<td>
                     <div width="780" class="alert alert-warning warning" align="center">
                     <h3>Warning! Please read the following and don&#39;t hesitate to contact our support if you need any help.</h3>
                     <ul>';
				if(!$mysql_status) {
					echo '<li>MySQL was not detected on your server but it doesn&#39;t mean it&#39;s not there. If you know that it is installed disregard this message, otherwise please ask your host to install it.</li>';
				}

               echo '</ul>
                     </div>
                     </td>
                     </tr>
                     </table>
                     </div>';
      } 

      if(!$ion_status || !$curl_status || !$fopen_status || !$file_gc_status || !validate_php_version(phpversion())) {

		echo '<div align="center">
               	<table width="780">
                 	<tr>
                   	<td>
                     <div width="780" class="alert alert-danger warning" align="center">
                     <h3>Warning! Please read the following and don&#39;t hesitate to contact our support if you need any help.</h3>
                     <ul>';

				if(!$ion_status) {
					echo '<li>Ion Cube Loader needs to be installed on your server for our software to run. Please ask your host to install it.</li>';
				}

				if(!$curl_status) {
					echo '<li>cURL needs to be enabled on your server for our software to run. Please ask your host to enable it.</li>';
				}

				if(!$fopen_status) {
					echo '<li>Fopen needs to be enabled on your server for our software to run. Please ask your host to install it.</li>';
				}

				if(!$file_gc_status) {
					echo '<li>file_get_contents needs to be enabled on your server for our software to run. Please ask your host to install it.</li>';
				}

				if(!validate_php_version(phpversion())) {
					echo '<li>Our scripts were not tested with your PHP version, they were tested with PHP 5.2, 5.3 and 5.4. They should work with other PHP versions but we cannot guarantee it.</li>';
				}

               echo '</ul>
                     </div>
                     </td>
                     </tr>
                     </table>
                     </div>';
      } 
      else {
		echo '<div align="center">
                    <table width="780">
                    <tr>
                    <td>
                    <div width="780" class="alert alert-success success" align="center">
                       <h3>If you have warnings please address them, otherwise everything looks good.</h3>
                    </div>
                    </td>
                    </tr>
                    </table>
                    </div>';
      }
    ?>

    </div>
  </body>
</html>
